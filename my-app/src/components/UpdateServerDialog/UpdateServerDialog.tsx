import React from "react";
import css from "./UpdateServerDialog.module.css";

type Props = {
  getServerGroupsLoading: boolean;
  getServerGroupsSuccess: boolean;
  getServerGroupsFailure: boolean;
  serverGroups?: { id: string; name: string }[];
  updateLoading: boolean;
  updateFailure: boolean;
  updateFailureMessage?: string;
  onUpdateClickHandler: () => void;
  onBackClickHandler: () => void;
  onNameChangeHandler: (e: React.ChangeEvent<HTMLInputElement>) => void;
  onServerGroupChangeHandler: (e: React.ChangeEvent<HTMLSelectElement>) => void;
};

const UpdateServerDialog = ({
  getServerGroupsLoading,
  getServerGroupsSuccess,
  getServerGroupsFailure,
  serverGroups,
  updateLoading,
  updateFailure,
  updateFailureMessage,
  onUpdateClickHandler,
  onBackClickHandler,
  onNameChangeHandler,
  onServerGroupChangeHandler,
}: Props) => {
  return (
    <div className={css.container}>
      <div className={css.row}>
        <label>
          Name:
          <input type="text" onChange={onNameChangeHandler} />
        </label>
      </div>

      <div className={css.row}>
        <label>
          Server Groups:
          <select
            defaultValue=""
            onChange={onServerGroupChangeHandler}
            disabled={getServerGroupsLoading}
          >
            <option value="">
              {getServerGroupsLoading
                ? "Loading options..."
                : "Select your option"}
            </option>
            {getServerGroupsFailure ? "Failed to retrieve server groups" : null}
            {getServerGroupsSuccess
              ? serverGroups?.map(({ id, name }) => (
                  <option key={id} value={id}>
                    {name}
                  </option>
                ))
              : null}
          </select>
        </label>
      </div>

      <div className={css.row}>
        <button
          type="button"
          onClick={onBackClickHandler}
          disabled={updateLoading}
        >
          Back
        </button>

        <button
          type="button"
          onClick={onUpdateClickHandler}
          disabled={updateLoading}
        >
          {updateLoading ? "Updating..." : "Update"}
        </button>
      </div>

      <div>{updateFailure ? updateFailureMessage : null}</div>
    </div>
  );
};

export default UpdateServerDialog;
