import {
  GET_SERVER_GROUPS_STARTED,
  GET_SERVER_GROUPS_SUCCESS,
  GET_SERVER_GROUPS_FAILURE,
  actionType,
} from "../types/getServerGroups";

const { REACT_APP_API_ENDPOINT_URI } = process.env;

type ServerGroup = {
  id: string;
  name: string;
};

type SuccessPayload = {
  data: ServerGroup[];
};

type ActionType = {
  type: actionType;
  payload?: SuccessPayload;
};

export const getServerGroupsStarted = (): ActionType => ({
  type: GET_SERVER_GROUPS_STARTED,
});

export const getServerGroupsSuccess = ({
  data,
}: SuccessPayload): ActionType => ({
  type: GET_SERVER_GROUPS_SUCCESS,
  payload: {
    data,
  },
});

export const getServerGroupsFailure = (): ActionType => ({
  type: GET_SERVER_GROUPS_FAILURE,
});

export const getServerGroupsRequest = () => async (
  dispatch: (actionType: ActionType) => void
) => {
  dispatch(getServerGroupsStarted());

  try {
    const url = `${REACT_APP_API_ENDPOINT_URI}/serverGroups`;
    const response = await fetch(url, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    });

    if (response.ok) {
      const data = await response.json();
      dispatch(getServerGroupsSuccess({ data }));
      return;
    }

    dispatch(getServerGroupsFailure());
  } catch {
    // network error
    dispatch(getServerGroupsFailure());
  }
};
