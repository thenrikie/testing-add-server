import getServerGroupsReducer, { State } from "../reducers/getServerGroups";
import { getServerGroupsRequest } from "../actions/getServerGroups";

import createMockStore from "./testUtils/createMockStore";
import nextTick from "./testUtils/nextTick";

describe("getServerGroups request", () => {
  let initState: State;
  let store: any;

  beforeAll(() => {
    initState = getServerGroupsReducer(undefined, {});
  });

  beforeEach(() => {
    store = createMockStore();
  });

  it("should return the initial state with no action", () => {
    expect(initState).toMatchSnapshot();
  });

  it("should pass correct parameters to fetch API", () => {
    fetchMock.mockResponseOnce("{}");
    store.dispatch(getServerGroupsRequest());

    expect(fetchMock.mock.calls).toMatchSnapshot();
  });

  it("should return state with loading=true when request started", async () => {
    fetchMock.mockResponseOnce("{}");

    store.dispatch(getServerGroupsRequest());
    expect(initState).toMatchDiffSnapshot(store.getState().getServerGroups);
  });

  it("should return state with success=true and server groups data when request succeed with HTTP 200", async () => {
    fetchMock.mockResponseOnce(
      JSON.stringify([
        {
          id: "id-1",
          name: "group1",
        },
        {
          id: "id-2",
          name: "group2",
        },
      ]),
      { status: 200 }
    );

    store.dispatch(getServerGroupsRequest());
    await nextTick();

    expect(initState).toMatchDiffSnapshot(store.getState().getServerGroups);
  });

  it("should return state with failure=true when request failed with 40x", async () => {
    fetchMock.mockResponseOnce("{}", { status: 400 });

    store.dispatch(getServerGroupsRequest());
    await nextTick();

    expect(initState).toMatchDiffSnapshot(store.getState().getServerGroups);
  });

  it ("should return state with failure=true on network error", async() => {
    fetchMock.mockAbortOnce();

    store.dispatch(getServerGroupsRequest());
    await nextTick();

    expect(initState).toMatchDiffSnapshot(store.getState().getServerGroups);
  });
});
