import {
  UPDATE_SERVER_STARTED,
  UPDATE_SERVER_SUCCESS,
  UPDATE_SERVER_FAILURE,
  UPDATE_SERVER_RESET,
  actionType,
} from "../types/updateServer";

const { REACT_APP_API_ENDPOINT_URI } = process.env;

type requestPayload = {
  serverUUID: string;
  name: string;
  serverGroupId: string;
};

type FailurePayload = {
  failureMessage: string;
};

type ActionType = {
  type: actionType;
  payload?: FailurePayload;
};

export const updateServerStarted = (): ActionType => ({
  type: UPDATE_SERVER_STARTED,
});

export const updateServerSuccess = (): ActionType => ({
  type: UPDATE_SERVER_SUCCESS,
});

export const updateServerFailure = (payload?: FailurePayload): ActionType => ({
  type: UPDATE_SERVER_FAILURE,
  payload: payload,
});

export const updateServerReset = (): ActionType => ({
  type: UPDATE_SERVER_RESET,
});

export const updateServerRequest = (payload: requestPayload) => async (
  dispatch: (actionType: ActionType) => void
) => {
  dispatch(updateServerStarted());

  const url = `${REACT_APP_API_ENDPOINT_URI}/server/${payload.serverUUID}`;

  try {
    const response = await fetch(url, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(payload),
    });

    if (response.ok) {
      dispatch(updateServerSuccess());
      return;
    }

    if (response.status === 404 || response.status === 400) {
      const { message } = await response.json();
      dispatch(updateServerFailure({ failureMessage: message }));
      return;
    }

    dispatch(updateServerFailure());
  } catch {
    // network error
    dispatch(updateServerFailure());
  }
};
